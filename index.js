const fetch = require('node-fetch');

/**
 * @class Client
 * @param {Number} port
 */
function Client({ port }){
    /**
     * @method execute
     * @param {Function} _function
     * @param {Object} params
     * @return {Promise<String>}
     */
    this.execute = (_function, params) =>
        new Promise((resolve, reject) =>
            fetch(`http://localhost:${port}`, {
                method: 'POST',
                body: `(${_function})(${JSON.stringify(params)})`
            }).then(res => res.text().then(body => ({
                200: resolve,
                400: reject
            })[res.status](body)).catch(reject)).catch(reject));
}

module.exports = Client;